certifi==2021.5.30; python_version >= "2.7" and python_full_version < "3.0.0" or python_full_version >= "3.6.0"
cffi==1.14.6; python_version >= "3.6"
charset-normalizer==2.0.4; python_full_version >= "3.6.0" and python_version >= "3"
cryptography==3.4.7; python_version >= "3.6"
django-extensions==3.1.3; python_version >= "3.6"
django==2.2.24; python_version >= "3.5"
idna==3.2; python_version >= "3.5" and python_full_version < "3.0.0" or python_full_version >= "3.6.0" and python_version >= "3.5"
mysqlclient==2.0.3; python_version >= "3.5"
pycparser==2.20; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.6"
pymysql==1.0.2; python_version >= "3.6"
pyopenssl==19.1.0
pytz==2021.1; python_version >= "3.6"
requests==2.26.0; (python_version >= "2.7" and python_full_version < "3.0.0") or (python_full_version >= "3.6.0")
six==1.16.0; python_version >= "2.7" and python_full_version < "3.0.0" or python_full_version >= "3.3.0"
sqlparse==0.4.1; python_version >= "3.6"
unidecode==1.2.0; (python_version >= "2.7" and python_full_version < "3.0.0") or (python_full_version >= "3.4.0")
urllib3==1.26.6; python_version >= "2.7" and python_full_version < "3.0.0" or python_full_version >= "3.6.0" and python_version < "4"
whitenoise==5.3.0; python_version >= "3.5" and python_version < "4"
